import React, { Component } from 'react'
import { store } from '../../store'
export default function(ComposedClass) {

  class AuthenticationCheck extends Component {
    constructor(props) {
      super()
    }

    componentDidMount() {
      let store_data = store.getState()
      let token = store_data.auth.token
   
      if (!token) {
        return this.props.history.replace(`/login`)
      }
    }

   

    render() {
      return <ComposedClass {...this.props} />
    }
  }

  return AuthenticationCheck
}
