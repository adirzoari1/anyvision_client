import React, { Component } from 'react'
import { Spin,Icon } from "antd";
import './Loader.css'

const Loader = (props)=>(
  <div
  className='loader'>
  <Spin indicator={<Icon type="loading" style={{ fontSize: 40,color:'black' }} spin />} />
</div>
)
export default Loader
  
