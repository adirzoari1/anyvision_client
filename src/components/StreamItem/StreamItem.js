import React, { Component } from "react";
import { Card, List } from "antd";
import "./StreamItem.css";
const { Meta } = Card;

class StreamItem extends Component {
    state = {
        loading: false
    };

    componentDidMount = ()=>{
        // for pre render for show loading card
        this.setState({loading:true})
    }
  
  render() {
    return (
      <List.Item>
        <Card  loading={this.state.loading} 
            cover={
                <iframe
                src={`https://wcs5-eu.flashphoner.com:8888/embed_player?urlServer=&streamName=${this.props.url}&mediaProviders=WebRTC,Flash,MSE,WSPlayer`}
                frameBorder="0"
                scrolling="no"
                allowFullScreen="allowfullscreen"
                onLoad={() => {this.setState({loading:false})}}
              ></iframe>
            }
        >
           <Meta
           title={this.props.title}
         />
        </Card>
      
        </List.Item> 
           );
  }
}
export default StreamItem;
