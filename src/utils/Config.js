export default {
    local: {
      API_URL: 'http://localhost:3014',
    },
    production: {
      API_URL: 'https://anyvision-server.herokuapp.com',
    },
  }
  