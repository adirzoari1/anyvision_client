import config from './Config'
import {store} from '../store'
class Rest {

  async send(auth, path, method, body) {
    const API_URL = config[process.env.REACT_APP_ENV].API_URL
    const headers = {}

    headers['Accept'] = 'application/json'
    headers["Content-Type"] = "application/json";

    let store_data = store.getState()
    let token = store_data.auth.token
    if (auth && token) {
      // headers["Authorization"] = "Bearer " + UserStore.token;
      headers['Authorization'] = token
    }
    let url = `${API_URL}${path ? path : ''}`
   
    try {
      let response = await fetch(url, {
        method: method,
        headers: headers,
        body: body != null ? JSON.stringify(body): null,
      })

      let resB = await response.json()
      if (response.status != 200) {
        return Promise.reject(resB)
      }
      return resB

     
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export default new Rest()
