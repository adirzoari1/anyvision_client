import Rest from './Rest'

class Api {

  // auth
  register(data) {
    return Rest.send(false, '/api/usr/register', 'POST', data)
  }

  login(data) {
    return Rest.send(false, '/api/usr/login', 'POST', data)
  }


  // streams
  addStream(data) {
    return Rest.send(true, '/api/stream/add-stream', 'POST', data)
  }
  getStreams(data) {
    return Rest.send(true, '/api/stream/get-streams-by-user-id', 'GET')
  }

  getStreamById(stream_id) {
    return Rest.send(true, `/api/stream/get-stream-by-id/${stream_id}`, 'GET')
  }

  watchStream(data) {
    return Rest.send(true, `/api/stream/watch-stream`, 'POST',data)
  }

}

export default new Api()
