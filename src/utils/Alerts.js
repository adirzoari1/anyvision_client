import { notification } from "antd"

export const success = (message,onCloseFunc) =>{
   notification.success({
       description:message,
       duration:1,
       onClose:()=>{
           if(onCloseFunc)
            onCloseFunc()
       }
   })
}

export const error = message =>{
    notification.error({
        description:message,
        duration:3
    })
 }