import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import React, { Component } from 'react'
import { AuthCheck } from '../components'
import { Layout } from "antd";

import { 
  Login,
  Register,
  App,

} from '../screens'
const { Header, Content, Footer, Sider } = Layout;

export default class Root extends Component {
  render() {
    return (
      <Layout className="container-root">
        <Content>
        <Router>
          <Route exact path='/login' component={Login} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/app/*' component={AuthCheck(App)} />
          <Route exact path='/app' component={AuthCheck(App)} />
          <Route exact path='/' render={() => <Redirect to='/app' />} />
      </Router>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Powered By Adir Zoari </Footer>

      </Layout>
    )
  }
}
