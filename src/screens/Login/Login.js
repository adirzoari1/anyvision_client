import React, { Component } from "react";
import { Form, Icon, Input, Button, notification } from "antd";
import { connect } from 'react-redux'
import { Link } from "react-router-dom";
import "./Login.css";
import {login} from '../../store/actions'
class Login extends Component {
  
  componentDidMount = ()=>{
   if(this.props.auth.token){
     this.props.history.push('/')
   }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
           this.props.login({email:values.email,password:values.password})
      }
    });
  };

 

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
          <div className="container-screen">
           
            <div className="form-wrapper">
            <img
              src={require("../../assets/images/logo.png")}
              className="logo"
            />
            <Form onSubmit={this.handleSubmit} className="form">
              <Form.Item hasFeedback>
                {getFieldDecorator("email", {
                  rules: [
                    { required: true, message: "email is required" },
                    { type: "email", message: "email is not valid" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    placeholder="Email"
                    className="form-input"
                  />
                )}
              </Form.Item>
              <Form.Item hasFeedback>
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "password is required" }
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    placeholder="Password"
                    className="form-input"
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="form-button"
                >
                  Log in
                </Button>
                <p>
                  Not yet a member? <Link to="/register">Sign up</Link>
                </p>
              </Form.Item>
            </Form>
            </div>
         
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  login: (data) => dispatch(login(data,ownProps.history))
})

export default connect(mapStateToProps,mapDispatchToProps)(Form.create({ name: "login" })(Login));
