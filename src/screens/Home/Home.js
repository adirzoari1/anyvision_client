import React, { Component,Fragment } from "react";
import { Form, Icon, Input, Button, Row,Alert } from "antd";
import { connect } from 'react-redux'
import { Link } from "react-router-dom";
import "./Home.css";
import {addStream,getStreams} from '../../store/actions'


class Home extends Component {

  componentDidMount = ()=>{
    this.props.getStreams()
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
          await this.props.addStream({title:values.title,url:values.url},this.props.history)
        
      }
    });
  };


 

  render() {

    const { getFieldDecorator } = this.props.form;
    return (
          <div className="container-screen">
            <div className="header">

            <h3>Hello, {this.props.auth.user.full_name}!</h3>
         
            <Row type="flex">
            {this.props.streams.length>0?
             <Fragment>
              <p>You have {this.props.streams.length} saved streams,</p>
              <Link to="/app/streams">Watch them</Link>
              </Fragment>
              :
            <Fragment>
              
              Your saved stream list is empty, please add!
              </Fragment>
              }
            </Row>
              </div>
            <div className="form-wrapper">
            <Form onSubmit={this.handleSubmit} className="form">
              <h1>Add rtsp stream</h1>
              <Form.Item>
                {getFieldDecorator("title", {
                  rules: [
                    { required: true, message: "title is required" },
                  ]
                })(
                  <Input
                    prefix={
                      <Icon type="edit" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    placeholder="Stream title"
                    className="form-input"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("url", {
                  rules: [
                    { required: true, message: "url is required" },
                    {pattern:"^((rtsp|mms)?://)(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?",message:'url is not valid'}
                  ]
                })(
                  <Input
                  prefix={
                    <Icon type="link" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Stream url"
                  className="form-input"
                />
                )}
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="form-button"
                >
                  Add stream
                </Button>
              
              </Form.Item>
            </Form>
            </div>
          </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
      streams: state.stream.streams,
      auth: state.auth,

  }
}
const mapDispatchToProps = (dispatch, ownProps) => ({
  addStream: (data) => dispatch(addStream(data,ownProps.history)),
  getStreams: () => dispatch(getStreams())
})


export default connect(mapStateToProps,mapDispatchToProps)(Form.create({ name: "home" })(Home));
