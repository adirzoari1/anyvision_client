import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Home, Streams} from '..'

export default class App extends Component {
  render() {
    return (
            <Router>
              <Switch>
                <Route exact path='/app' component={Home} />
                <Route exact path='/app/streams' component={Streams} />
              </Switch>
            </Router>

    )
  }
}

