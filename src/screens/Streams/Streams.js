import React, { Component } from "react";
import { Row, List, Card } from "antd";
import { connect } from "react-redux";
import "./Streams.css";
import { getStreams } from "../../store/actions";
import { StreamItem } from "../../components";
class Streams extends Component {

  render() {
  
    return (
      <div className="container-screen">
        <h1>My Stream Videos</h1>
        <div className="container-streams">
          <List
            grid={{ gutter: 12, column: 5 }}
            dataSource={this.props.streams}
            renderItem={item => (
              <StreamItem rowKey={item.stream_id} {...item} />
            )}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    streams: state.stream.streams
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  getStreams: data => dispatch(getStreams(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Streams);
