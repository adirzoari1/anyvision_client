import React, { Component } from "react";
import ReactDOM from "react-dom";
import Root from "./routes";
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'

import * as serviceWorker from "./serviceWorker";

import "./index.scss";
import { store, persistor} from './store'
import { Loader } from "./components";

class Main extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loader/>} persistor={persistor}>
        <div className="container-root">
          <Root />
        </div>
        </PersistGate>

      </Provider>
    );
  }
}

ReactDOM.render(<Main />, document.getElementById("root"));
serviceWorker.unregister();
