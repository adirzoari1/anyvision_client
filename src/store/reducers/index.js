import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage' 
import { persistReducer } from 'redux-persist'

import auth  from './auth.reducer';
import stream  from './stream.reducer';

const rootPersistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['auth','stream'],
}
const authPersistConfig = {
	key: 'auth',
	storage: storage,
  };
  const streamPersistConfig = {
    key: 'stream',
    storage: storage,
  };
  


  

const rootReducer = combineReducers({
  auth,
  stream
});

export default persistReducer(rootPersistConfig, rootReducer)
