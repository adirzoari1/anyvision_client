import { ADD_STREAM, SET_STREAMS } from "../types/stream.types";
import { REHYDRATE } from 'redux-persist';

const initialState = {
  streams: []
};

export default (state = initialState, action) => {
  switch (action.type) {
 
    case ADD_STREAM:
      return {
        ...state,
        streams: [...state.streams,action.payload]
      };
    case SET_STREAMS:
      return {
        ...state,
        streams: action.payload
      };
    default:
      return state;
  }
};
