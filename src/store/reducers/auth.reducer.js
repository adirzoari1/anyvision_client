import { SET_USER } from '../types'
import { REHYDRATE } from 'redux-persist';

const initialState = {
	token: null,
	user: {}
}

export default (state = initialState, action) => {
	switch (action.type) {
	
		case SET_USER:
			return {
				...state,
				token:action.payload.token,
				user: {
					email:action.payload.email,
					user_id:action.payload.user_id,
					full_name:action.payload.full_name
				}
			}
		default:
			return state
	}
}

