import {SET_STREAMS, ADD_STREAM } from '../types'
import Api from '../../utils/Api'
import { success, error } from '../../utils/Alerts'

export  const addStream = (data,history) =>{
    return async(dispatch)=>{
            try {
                let response = await Api.addStream(data) 
                dispatch(addNewStream(response.data))
                success('Stream inserted successfully',()=>{  history.push('/app/streams')})
               
            }catch(e){
               error(e.msg)
            }
           
    }
}

export const getStreams = _ =>{
    return async(dispatch)=>{
            let response = await Api.getStreams()
            dispatch(setStreams(response.data))
       
    }
}

const addNewStream = stream =>({
    type:ADD_STREAM,
    payload:stream
})

const setStreams = streams =>({
    type:SET_STREAMS,
    payload:streams
})


