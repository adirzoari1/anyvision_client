import {SET_USER } from '../types'
import Api from '../../utils/Api'
import { success,error}  from '../../utils/Alerts'

export  const login = (data,history) =>{
    return async(dispatch)=>{
            try {
                let response = await Api.login(data)
                successAuth(dispatch,'You are successfully logged in',response.data,history)

            }catch(e){
                error(e.msg)
            }
           
    }
}

export const register = (data,history) =>{
    return async(dispatch)=>{
        try {
            let response = await Api.register(data)
            successAuth(dispatch,'You are successfully registered',response.data,history)
        }catch(e){
            error(e.msg)
        }
       
    }
}
export const successAuth = (dispatch,message,data,history)=>{
    success(message,()=>{history.push('/')})
    dispatch(setUserDetails(data))    
   
}
export const setUserDetails = (data)=>({
    type:SET_USER,
    payload:data
})

