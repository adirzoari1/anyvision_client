import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import { persistStore  } from "redux-persist";
import rootReducer from "./reducers";
const loggerMiddleware = createLogger();

const store = createStore(rootReducer, applyMiddleware(thunk, loggerMiddleware));
const persistor = persistStore(store);
export { store, persistor };
